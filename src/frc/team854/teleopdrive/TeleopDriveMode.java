package frc.team854.teleopdrive;

public enum TeleopDriveMode {
	SIMPLE, SCALED
}
