/*
 * Class: RobotStructureConstants
 * Author: Everyone
 * Date: 06/01/2018
 * Description: Contains constants related to the robot's physical structure, such as distance between wheels.
 */

package frc.team854.constants;

public interface RobotStructureConstants {

}
