/**
 * Name: Direction 1D
 * Authors: Julian Dominguez-Schatz
 * Date: 17/02/2018
 * Description: Defines possible directions along a single axis.
 */

package frc.team854.utils;

public enum Direction1D {
	FORWARD, REVERSE, OFF
}
